package com.ht117.float_head.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ht117.float_head.R;
import com.ht117.float_head.models.UserModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by PQHy on 12/29/2016.
 */

public class UserAdapter extends ArrayAdapter<UserModel> {

    private List<UserModel> users;

    public UserAdapter(Context context) {
        super(context, 0);
        users = new ArrayList<>();
        String[] arr = context.getResources().getStringArray(R.array.rnd_names);
        for (int i=0;i<arr.length;i++) {
            UserModel model = new UserModel();
            model.setName(arr[i]);
            users.add(model);
        }
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Nullable
    @Override
    public UserModel getItem(int position) {
        return users.get(position);
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public boolean isEmpty() {
        return users.size() == 0;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        UserModel model = users.get(position);

        View view = convertView;
        UserHolder holder = null;
        if (view == null) {
            // inflate view
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_user, null);
            holder = new UserHolder(view);
            view.setTag(holder);
        } else {
            holder = (UserHolder) view.getTag();
        }

        holder.chatName.setText(model.getName());
        holder.avatar.setBackgroundResource(R.drawable.head_ninja);

        return view;
    }

    class UserHolder {
        TextView chatName;
        CircleImageView avatar;
        UserHolder(View view) {
            chatName = (TextView) view.findViewById(R.id.chatName);
            avatar = (CircleImageView) view.findViewById(R.id.userAvatar);
        }
    }
}

