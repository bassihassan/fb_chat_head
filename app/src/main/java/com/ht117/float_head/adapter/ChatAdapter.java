package com.ht117.float_head.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ht117.float_head.R;
import com.ht117.float_head.models.ChatModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by PQHy on 12/29/2016.
 */

public class ChatAdapter extends ArrayAdapter<ChatModel> {

    final int YOU = 1;
    final int OTHER = 2;
    private List<ChatModel> chats;

    public ChatAdapter(Context context) {
        super(context, 0);
        chats = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            ChatModel model = new ChatModel();
            model.setContent("Message ..." + i);
            model.setFrom("Steve");
            model.setCreatedAt(new Date().getTime());
            chats.add(model);
        }

        ChatModel model = new ChatModel();
        model.setFrom(null);
        model.setContent("Message ... shit");
        model.setCreatedAt(new Date().getTime());
        chats.add(model);
    }

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public boolean isEmpty() {
        return chats.size() == 0;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ChatHolder holder = null;

        if (view == null) {
            if (getItemViewType(position) == YOU) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.item_chat_right, null);
            } else {
                view = LayoutInflater.from(getContext()).inflate(R.layout.item_chat_left, null);
            }
            holder = new ChatHolder(view);
            view.setTag(holder);
        } else {
            holder = (ChatHolder) view.getTag();
        }
        ChatModel model = chats.get(position);
        holder.content.setText(model.getContent());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
        holder.sentAt.setText(sdf.format(new Date(model.getCreatedAt())));

        return view;
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel model = chats.get(position);

        // simulate that message from you is null
        return model.getFrom() == null ? YOU : OTHER;
    }

    class ChatHolder {
        TextView content;
        TextView sentAt;
        CircleImageView avatar;

        public ChatHolder(View view) {
            content = (TextView) view.findViewById(R.id.chatContent);
            avatar = (CircleImageView) view.findViewById(R.id.chatAvatar);
            sentAt = (TextView) view.findViewById(R.id.sentAt);
        }
    }
}
