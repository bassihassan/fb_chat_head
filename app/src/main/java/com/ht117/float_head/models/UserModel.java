package com.ht117.float_head.models;

/**
 * Created by PQHy on 12/29/2016.
 */

public class UserModel {
    private String name;
    private String uri;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
