package com.ht117.float_head;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ht117.float_head.module.FloatService;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    final String TAG = "TheShitCode";
    final String ID = "id";
    final String NAME = "name";
    private String[] names;
    final int OVERLAY_PER = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnStart).setOnClickListener(this);
        names = getResources().getStringArray(R.array.rnd_names);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                initFloating();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PER) {
            if (resultCode == RESULT_OK) {
                startFloating();
            }
        }
    }

    private void initFloating() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, OVERLAY_PER);
        } else {
            startFloating();
        }
    }

    private void startFloating() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(TAG);
                int id = new Random().nextInt(names.length);
                intent.putExtra(ID, id);
                intent.putExtra(NAME, names[id]);
                sendBroadcast(intent);
            }
        }, 1000*10);

        // Start service
        Intent intent = new Intent(this, FloatService.class);
        finish();
        startService(intent);
    }
}
