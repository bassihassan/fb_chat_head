package com.ht117.float_head.module;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ht117.float_head.R;
import com.ht117.float_head.adapter.ChatAdapter;
import com.ht117.float_head.adapter.UserAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by steve on 12/28/16.
 */

public class FloatService extends Service implements View.OnTouchListener,
    View.OnClickListener {

    final String TAG = "TheShitCode";
    final String ID = "id";
    final int THRES_HOLD = 5;
    final int APP = -1;
    private View rootView;
    private View headerView;
    private View expandedView;
    private WindowManager windowManager;
    private WindowManager.LayoutParams params;
    private int initX, initY;
    private float touchX, touchY;
    private List<Integer> ids;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        ids = new ArrayList<>();
        rootView = LayoutInflater.from(this).inflate(R.layout.widget_floating, null);

        headerView = rootView.findViewById(R.id.header);
        expandedView = rootView.findViewById(R.id.expanded_view);

        headerView.findViewById(R.id.headApp).setOnClickListener(this);
        headerView.findViewById(R.id.headApp).setOnTouchListener(this);
        headerView.findViewById(R.id.btnClose).setOnClickListener(this);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 50;
        params.y = 100;

        windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
        windowManager.addView(rootView, params);

        registerReceiver(receiver, new IntentFilter(TAG));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (rootView != null) {
            windowManager.removeView(rootView);
        }
        unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                stopSelf();
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initX = params.x;
                initY = params.y;
                touchX = event.getRawX();
                touchY = event.getRawY();
                return true;
            case MotionEvent.ACTION_MOVE:
                params.x = initX + (int)(event.getRawX() - touchX);
                params.y = initY + (int)(event.getRawY() - touchY);
                windowManager.updateViewLayout(rootView, params);
                return true;
            case MotionEvent.ACTION_UP:
                int xDiff = (int)(event.getRawX() - touchX);
                int yDiff = (int)(event.getRawY() - touchY);

                // clicked case
                if (xDiff < THRES_HOLD && yDiff < THRES_HOLD) {
                    Log.d(TAG, "Touched " + v.getId());
                    if (v.getId() == R.id.headApp) {
                        initChatHead(APP);
                    } else {
                        initChatHead(v.getId());
                    }
                }
                break;
        }
        return false;
    }

    private boolean isExpanded() {
        return expandedView.getVisibility() == View.VISIBLE ? true : false;
    }

    private void initChatHead(int id) {
        if (id == APP) {
            ((TextView)expandedView.findViewById(R.id.chatId)).setText("ChatList");
        } else {
            ((TextView)expandedView.findViewById(R.id.chatId)).setText(getResources().getStringArray(R.array.rnd_names)[id]);
        }

        ListView lv = (ListView) expandedView.findViewById(R.id.chatView);

        if (isExpanded()) {
            if (id == APP) {
                expandedView.setVisibility(View.GONE);
            }
        } else {
            expandedView.setVisibility(View.VISIBLE);
            if (id == APP) {
                UserAdapter userAdapter = new UserAdapter(this);
                lv.setAdapter(userAdapter);
            } else {
                lv.setAdapter(null);
                ChatAdapter chatAdapter = new ChatAdapter(this);
                lv.setAdapter(chatAdapter);
            }
        }
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle data = intent.getExtras();
            int id = data.getInt(ID);
            ImageView img = new ImageView(FloatService.this);
            img.setId(id);
            img.setTag(TAG);
            img.setBackgroundResource(R.drawable.head_ninja);
            img.setOnClickListener(FloatService.this);
            img.setOnTouchListener(FloatService.this);

            int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
            LinearLayout.LayoutParams paraImg = new LinearLayout.LayoutParams(size, size);
            paraImg.setMargins(10, 0, 0, 0);

            ((ViewGroup)headerView).addView(img, paraImg);
            ids.add(id);
        }
    };
}
